Source: konquest
Section: games
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Daniel Schepler <schepler@debian.org>,
           Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 6.0.0~),
               gettext,
               libkdegames6-dev,
               libkf6config-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6crash-dev (>= 6.0.0~),
               libkf6dbusaddons-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6guiaddons-dev (>= 6.0.0~),
               libkf6i18n-dev (>= 6.0.0~),
               libkf6widgetsaddons-dev (>= 6.0.0~),
               libkf6xmlgui-dev (>= 6.0.0~),
               pkg-kde-tools,
               qt6-base-dev (>= 6.5.0~),
               qt6-scxml-dev (>= 6.5.0~),
               qt6-svg-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/konquest
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/konquest.git

Package: konquest
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Breaks: ${kde-l10n:all},
Replaces: ${kde-l10n:all},
Recommends: khelpcenter,
Description: simple turn-based strategy game
 Konquest is a game of galactic conquest for KDE, where rival empires vie to
 conquer planets and crush all opposition.  The game can be played with up to
 nine empires, commanded either by the computer or by puny earthlings.
 .
 This package is part of the KDE games module.
